const express = require('express'); // referencia al paquete express
const userFile = require('./user.json');
const userFile2 = require('./data/user.json'); // para el login y logout
const bodyPaser = require('body-parser'); // esta en node_modelus
const requestJSON = require('request-json');
var app = express(); // creamos el servidor de Node
app.use(bodyPaser.json()); // digo que habilite el formateador a json
const port =3002;
//obtengo puerto
//var port = proccess.env.PORT || 3000;
const URLbase = '/colapi/v3/';

const baseMLabURL = 'https://api.mlab.com/api/1/databases/techu7db/collections/';
const apikeyMLab =  'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';


// GET users a través de mLab
app.get(URLbase + 'users',
  function(req, res) {
    const httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    const fieldParam = 'f={"_id":0}&';
   httpClient.get('user?' + fieldParam+ apikeyMLab,
      function(err, respuestaMLab, body) {
        console.log('Error: ' + err);
        console.log('Respuesta MLab: ' + respuestaMLab);
        console.log('Body: ' + body);
        var response = {};
        if(err) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      });
});


// GET users a través de mLab - filtrando solo un usuario
app.get(URLbase + 'users',
  function(req, res) {
    const httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    const fieldParam = 'f={"_id":0}&';
   httpClient.get('user?' + fieldParam+ apikeyMLab,
      function(err, respuestaMLab, body) {
        console.log('Error: ' + err);
        console.log('Respuesta MLab: ' + respuestaMLab);
        console.log('Body: ' + body);
        var response = {};
        if(err) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      }); //httpClient.get(
});

// GET accounts a través de mLab
app.get(URLbase + 'accounts/:id',
  function(req, res) {
    const httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    let id = req.params.id;
    const fieldParam = 'f={"_id":0}&';
    const queryParam = 'q={"userID":'+ id +'}&' ;
   httpClient.get('account?' + queryParam + fieldParam + apikeyMLab,
      function(err, respuestaMLab, body) {
        console.log('Error: ' + err);
        console.log('Respuesta MLab: ' + respuestaMLab);
        console.log('Body: ' + body);
        var response = {};
        if(err) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      }); //httpClient.get(
});

// GET user_account a través de mLab
app.get(URLbase + 'user-account/:id',
  function(req, res) {
    const httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    let id = req.params.id;
    const fieldParam = 'f={"_id":0}&';
    const queryParam = 'q={"id_user_account":'+ id +'}&' ;
   httpClient.get('user_account?' + queryParam + fieldParam + apikeyMLab,
      function(err, respuestaMLab, body) {
        console.log('Error: ' + err);
        console.log('Respuesta MLab: ' + respuestaMLab);
        console.log('Body: ' + body);
        var response = {};
        if(err) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      }); //httpClient.get(
});

// login con mLab
app.post(URLbase + "login",
function(req, res) {
  const httpClient = requestJSON.createClient(baseMLabURL);
  console.log("Cliente HTTP mLab creado.");
  var email= req.body.email;
  var pass= req.body.password;
  const fieldParam = 'f={"_id":0}&';
  const queryParam = 'q={"email":"' + email + '", "password": "'+ pass +'"}&';
  httpClient.get('user?' + queryParam + fieldParam + apikeyMLab,
     function(err, respuestaMLab, body) {
       console.log('Error: ' + err);
       console.log('Respuesta MLab: ' + respuestaMLab);
       console.log('Body: ' + body);

       var respuesta = body[0];
       console.log(respuesta);
       if(respuesta!=undefined){
           if (respuesta.password == pass) {
             console.log("Login Correcto");
             var session={"logged":true};
             var login = '{"$set":' + JSON.stringify(session) + '}';
             console.log(baseMLabURL+'user?q={"id": ' + respuesta.id + '}&' + apikeyMLab);
             httpClient.put('user?q={"id": ' + respuesta.id + '}&' + apikeyMLab, JSON.parse(login),
              function(errorP, respuestaMLabP, bodyP) {
                res.send(body[0]);
             });
           }
           else {
            res.send({"msg":"contraseña incorrecta"});
           }
         }else{
         console.log("Email Incorrecto");
         res.send({"msg": "email Incorrecto"});
         }
    });
});

// logout con mLab
app.post(URLbase + "logout",
function(req, res) {
  const httpClient = requestJSON.createClient(baseMLabURL);
  console.log("Cliente HTTP mLab creado.");
  var email= req.body.email;
  const fieldParam = 'f={"_id":0}&';
  const queryParam = 'q={"email":"' + email + '"}&';
  httpClient.get('user?' + queryParam + fieldParam + apikeyMLab,
     function(err, respuestaMLab, body) {
       console.log('Error: ' + err);
       console.log('Respuesta MLab: ' + respuestaMLab);
       console.log('Body: ' + body);

       var respuesta = body[0];
       console.log(respuesta);
       if(respuesta!=undefined){
          if (respuesta.logged == true){
            console.log("logout Correcto");
            var session={"logged":true};
            var logout = '{"$unset":' + JSON.stringify(session) + '}';
            console.log(logout);
            httpClient.put('user?q={"id": ' + respuesta.id + '}&' + apikeyMLab, JSON.parse(logout),
            function(errorP, respuestaMLabP, bodyP) {
              res.send(body[0]);
            });
          }else {
           console.log("Usuario no logueado");
           res.send({"msg": "usuario no logueado"});
          }
        }else{
         console.log("Usuario no existe");
         res.send({"msg": "usuario no existe"});
        }
    });
});





//Servidor escuchara en la ural (servidor local)
// http://localhost:3000/holamundo
app.listen(port,function() {
  console.log('Node JS escuchando en los puerto 3000');
});
