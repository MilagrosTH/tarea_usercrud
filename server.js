const express = require('express'); // referencia al paquete express
const userFile = require('./user.json');
const userFile2 = require('./data/user.json'); // para el login y logout
const bodyPaser = require('body-parser'); // esta en node_modelus
var app = express(); // creamos el servidor de Node
app.use(bodyPaser.json()); // digo que habilite el formateador a json
const port =3000;
//obtengo puerto
//var port = proccess.env.PORT || 3000;
const URL_BASE = '/api-peru/v1/';

// LOGIN - user.json - tarea del login y logout

 app.post(URL_BASE + 'login',
  function(request, response) {
  // console.log("POST /apitechu/v1/login");
   console.log(request.body.email);
   console.log(request.body.password);
   var user = request.body.email;
   var pass = request.body.password;
   for(us of userFile2) {
    if(us.email == user) {
     if(us.password == pass) {
      us.logged = true; // creo atributo tipo booleano como "ok"
      writeUserDataToFile(userFile2); // escribo en el archivo
      console.log("Login correcto!");
      response.send({"msg" : "Login correcto.",
              "idUsuario" : us.id,
               "logged" : "true"});
     } else {
      console.log("Login incorrecto.");
      response.send({"msg" : "Login incorrecto."});
     }
    }
   }
   console.log("Login incorrecto, usuario no existe.");
   response.send({"msg" : "Login incorrecto, usuario no existe."});
 });
 // LOGOUT - users.json
 app.post(URL_BASE + 'logout',
  function(request, response) {
   //console.log("POST /apitechu/v1/logout");
   var userId = request.body.id;
   for(us of userFile2) {
    if(us.id == userId) {
     if(us.logged) {
      delete us.logged; // borramos propiedad 'logged'
      writeUserDataToFile(userFile2);
      console.log("Logout correcto!");
      response.send({"msg" : "Logout correcto.", "idUsuario" : us.id});
     } else {
      console.log("Usuario no logueado.");
      response.send({"msg" : "Usuario no logueado."});
     }
   }
   }
   console.log("Logout incorrecto, usuario no existe.");
   response.send({"msg" : "Logout incorrecto, usuario no existe."});

 });
 function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data); // convierte a cadena
  fs.writeFile("./data/user.json", jsonUserData, "utf8",    // escribo en el archivo
   function(err) { //función manejadora para gestionar errores de escritura
    if(err) {
     console.log(err);
    } else {
     console.log("Datos escritos en 'data/user.json'.");
    }
   })
 }

//A partir de ahora ---- Todo cambia :P Tarea get post

//obtengo todos los usuarios total
app.get(URL_BASE + 'users',
   function(request,response) {
     response.status(200);
     response.send(userFile);
});

// Operacion Get a un usuario con ID(instance)
app.get (URL_BASE + 'users/:id',
  function(request,response){
    let indice = request.params.id;
    let respuesta = (userFile[indice-1] == undefined)? { "msg":"No existe"} : userFile[indice-1];
    response.status(200).send(respuesta);
});

//obtengo el numero de elementos total
app.get (URL_BASE + 'usersElement',
  function(request,response){
    var elemt = userFile.length;
    var jsonDuro = '{"num_elem":"'+elemt+'"}';
    var j = JSON.parse(jsonDuro);
    response.status(200);
    console.log(userFile.length);
    response.send(j);
    // otra forma de enviar
    // response.status(200).send(j);
//  response.send('Hola mundo 2');
});

//Operacion POST a users ----------------------------------------
app.post(URL_BASE + 'users',
  function(req,res){
    //acceso al body de la peticion
    let indice = userFile.length + 1; // indice para que no se repita
    console.log(req.body);
    console.log(req.body.id);
    let newUser = {
      id : indice,
      first_name :  req.body.first_name,
      last_name : req.body.last_name,
      email : req.body.email,
      password : req.body.password
    }
    userFile.push(newUser);
    res.status(201);
    res.send({"mensaje":"Usario creado correctamente", "usuario": newUser,
              "userFile actualizado":userFile})
});

//elimina el usuario ----------------------------
app.delete (URL_BASE + 'users/:id',
  function(request,response){
    let indice = request.params.id;
    let respuesta = (userFile[indice-1] == undefined)? { "msg":"No existe"} :
    { "Usuario Eliminado" : (userFile.splice(indice-1,1))};
    response.status(200).send(respuesta);
});

// actualizo modificando el ID
app.put (URL_BASE + 'users/:id',
  function(req,res){
    let indice = req.params.id;
    let newUser = {
      id : parseInt(req.params.id),
      first_name :  req.body.first_name,
      last_name : req.body.last_name,
      email : req.body.email,
      password : req.body.password
    }
    //let respuesta = (userFile[indice-1] == undefined)? { "msg":"No existe"} : userFile[indice-1];
    if (userFile[indice-1] == undefined){
      res.status(500).send({ "msg":"No existe"});
    }else{
      res.status(200);
      userFile.splice(indice-1, 1, newUser);
      res.send({"mensaje":"Usario modificado correctamente", "usuario": newUser,
                "userFile actualizado":userFile})
    }
});


// obtengo la cantidad de usuarios
app.get (URL_BASE + 'userssq',
  function(request,response){
    console.log(request.query);
    let query = request.query;
    console.log(query.max);
    response.status(200);

    //var lista = [];
    //for {
    //    lista.push(userFile[i]);
    //}
    //responde.send(lista);
});

// Operacion Get a un usuario con ID(instance)
app.get (URL_BASE + 'users/:id/:id2',
  function(request,response){
    let indice = request.params.id;
    let indice2 = request.params.id2;
    console.log(indice);
    console.log(indice2);
    response.status(200);
    //response.send(userFile[indice-1]);
    response.send(userFile[indice2-1]);
//  response.send('Hola mundo 2'); |@#~½¬{[]}
});



// A partir de aqui el mlab con mongoDB




//Servidor escuchara en la ural (servidor local)
// http://localhost:3000/holamundo
app.listen(port,function() {
  console.log('Node JS escuchando en los puerto 3000');
});
